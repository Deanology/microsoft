﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Microsoft.Models
{
    public class Album
    {
        public int AlbumId { get; set; }
        [Required()]
        [StringLength(100, MinimumLength = 3)]
        public string Title { get; set; }
        public decimal Price { get; set; }
        public Artist Artist { get; set; }
    }

}