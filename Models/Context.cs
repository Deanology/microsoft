﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Microsoft.Models
{
    public class Context : DbContext
    {
        public Context() : base("Microsoft")
        {
            /*Database.SetInitializer<Context>(new ContextInitializer());*/
        }
        public DbSet<Artist> Artists { get; set; }
        public DbSet<Album> Albums { get; set; }
    }
}