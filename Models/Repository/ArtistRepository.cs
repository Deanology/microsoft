﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Microsoft.Models.Repository
{
    public class ArtistRepository : Repository<Artist>
    {
        public List<Artist> GetByTitle(string name)
        {
           return  dbSet.Where(a => a.Name.Contains(name)).ToList();
        }
    }
}