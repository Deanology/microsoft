﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Microsoft.Models.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private Context context;
        protected DbSet<T> dbSet;

        public Repository()
        {
            context = new Context();
            dbSet = context.Set<T>();
        }
        public IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }
        public T GetById(object id)
        {
            return dbSet.Find(id);
        }
        public void Insert(T obj)
        {
            dbSet.Add(obj);
        }
        public void Update(T obj)
        {
            context.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(object id)
        {
            T getObjById = dbSet.Find(id);
            dbSet.Remove(getObjById);
        }
        public void Save()
        {
            context.SaveChanges();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.context != null)
                {
                    this.context.Dispose();
                    this.context = null;
                }
            }
        }
    }

    /*public class Repository<T> where T : class
    {
        private Context context = null;
        protected DbSet<T> DbSet { get; set; }
        public Repository()
        {
            context = new Context();
            DbSet = context.Set<T>();
        }
        public Repository(Context context)
        {
            this.context = context;
        }
        public List<T> GetAll()
        {
            return DbSet.ToList();
        }
        public T Get(int id)
        {
            return DbSet.Find(id);
        }
        public void Add(T entity)
        {
            DbSet.Add(entity);
        }
        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }*/
}