﻿using Microsoft.Models;
using Microsoft.Models.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Microsoft.Controllers
{
    public class GeneralController : Controller
    {
        private ArtistRepository _repository = null;
        public GeneralController()
        {
            this._repository = new ArtistRepository();
        }
        public ActionResult Index()
        {
            var artists = _repository.GetAll();
            return View(artists);
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Artist artist)
        {
            if (ModelState.IsValid)
            {
                _repository.Insert(artist);
                _repository.Save();
                return RedirectToAction("Index");
            }
            else
                return View(artist);
        }
        public ActionResult Edit(int id)
        {
            var artist = _repository.GetById(id);
            return View(artist);
        }
        [HttpPost]
        public ActionResult Edit(Artist artist)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(artist);
                _repository.Save();
                return RedirectToAction("Index");
            }
            else
            {
                return View(artist);
            }
        }
        public ActionResult Details(int id)
        {
            var artist = _repository.GetById(id);
            return View(artist);
        }
        public ActionResult Delete(int id)
        {
            var artist = _repository.GetById(id);
            return View(artist);
        }
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            var artist = _repository.GetById(id);
            _repository.Delete(id);
            _repository.Save();
            return RedirectToAction("Index");
        }
    }
    /*public class GeneralController : Controller
    {
        *//*Context context = new Context();*//*
        ArtistRepository repository = new ArtistRepository();
        AlbumRepository repository2 = new AlbumRepository();
        // GET: Album
        public ActionResult Details(int id)
        {
            Artist artist = repository.Get(id);
            if (artist == null)
                return HttpNotFound();
            else
                return View(artist);
        }
        public ActionResult Index()
        {
            return View(repository.GetAll());
           *//* return View();*//*
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Artist artist)
        {
            if (!ModelState.IsValid) return View(artist);

            //context.Artists.Add(new Artist() { Name = "Artist One" });
            //context.SaveChanges();
            repository.Add(new Artist() { Name = "Artist One" });
            repository.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult View(Album album)
        {
            if (ModelState.IsValid) return View(album);
            repository2.Add(new Album() { Title = "Twicw as tall" });
            repository2.SaveChanges();
            return RedirectToAction("Index");
        }
    }*/
}